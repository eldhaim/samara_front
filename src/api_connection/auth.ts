import { Injectable } from '@angular/core';
import { GlobalService } from '../app/global.service';
import axios, { AxiosError } from 'axios';
import { SingUpUser } from '../pages/interface_objects/sing_up';

@Injectable({ providedIn: 'root' })
export class AuthService {
  apiPrefix: string = 'users/user';
  constructor(private globalService: GlobalService) {}

  async login(username: string, password: string, user:boolean=false): Promise<boolean> {
    const authUrl = this.globalService.getAuthUrl();
    const authData = {
      username: username,
      password: password,
      grant_type: this.globalService.getGrantType(),
    };
    const authHeaders = this.globalService.getAuthHeaders();

    try {
      const authResponse = await axios.post(
        authUrl,
        new URLSearchParams(authData),
        { headers: authHeaders }
      );

      this.globalService.setToken(authResponse.data.access_token, user);
      return true; // Autenticación exitosa
    } catch (error: any) {
      console.error('Error en la autenticación:', error.message);

      if (axios.isAxiosError(error)) {
        // Manejar errores específicos de Axios
        const axiosError = error as AxiosError;
        console.error('Código de error:', axiosError.response?.status);
      }

      return false; // Autenticación fallida
    }
  }
  async signUp(singUpUser: SingUpUser): Promise<boolean> {
    try{
        const newUser = await axios.post(
            `${this.globalService.getApiUrl()}${this.apiPrefix}`,
            singUpUser,
            {
                headers: this.globalService.getApiHeaders()
            }
        );
        console.log("newUser");
        console.log(newUser);
        return true;
    }catch (error: any) {
        console.error('Error en la autenticación:', error.message);
  
        if (axios.isAxiosError(error)) {
          // Manejar errores específicos de Axios
          const axiosError = error as AxiosError;
          console.error('Código de error:', axiosError.response?.status);
          console.error('Código de error:', axiosError.response?.data);
        }
  
        return false; // Autenticación fallida
      }
  }
}
