import {Injectable} from '@angular/core';
import {GlobalService} from '../app/global.service';
import axios, {AxiosError} from 'axios';

@Injectable({providedIn: 'root'})
export class ProfilesService {
    apiPrefix: string = 'business/profile/';
    constructor(private globalService : GlobalService) {}

    async getProfile(profileId : string): Promise<object> {
        try{
            const profileResponse = await axios.get(
                `${this.globalService.getApiUrl()}${this.apiPrefix}?_id=${profileId}`,
                {
                    headers: this.globalService.getApiHeaders()
                }
            );
            console.log("profileResponse");
            console.log(profileResponse);
            return profileResponse.data;
        }catch (error: any) {
            console.error('Error en la autenticación:', error.message);
      
            if (axios.isAxiosError(error)) {
              // Manejar errores específicos de Axios
              const axiosError = error as AxiosError;
              console.error('Código de error:', axiosError.response?.status);
              console.error('Código de error:', axiosError.response?.data);
            }
      
            return {}; // Autenticación fallida
          }
    }
}