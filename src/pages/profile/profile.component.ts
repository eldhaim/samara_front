import { Component } from '@angular/core';
import { NavComponent } from '../general_components/nav/nav.component';
import { NgOptimizedImage } from '@angular/common';
import { ProfilesService } from '../../api_connection/profile';
import { GlobalService } from '../../app/global.service';
import { Commons } from '../../app/commons';

@Component({
    selector: 'app-profile',
    standalone: true,
    templateUrl: './profile.component.html',
    styleUrl: './profile.component.css',
    imports: [NavComponent, NgOptimizedImage]
})
export class ProfileComponent {
  private commons: Commons = new Commons();
  constructor(private globalService: GlobalService, private profilesService: ProfilesService) {}
  ngOnInit(): void {
    this.profileData();
  }
  async profileData(): Promise<void> {
    console.log(this.globalService.getProfileData());
    if(this.commons.objectIsEmpty(this.globalService.getProfileData())){
        this.globalService.setProfileData(await this.profilesService.getProfile(this.globalService.getProfileId()));
    }
  }
}
