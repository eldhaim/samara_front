import { Component } from '@angular/core';
import { GlobalService } from '../../app/global.service'; 
import { FormControl, FormGroup } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms'; 
import { Router } from '@angular/router'; 
import { NavComponent } from "../general_components/nav/nav.component"; 
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'; 
import { CommonModule } from '@angular/common';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { AuthService } from '../../api_connection/auth';

@Component({
  selector: 'app-sing-in',
  standalone: true,
  imports: [ReactiveFormsModule, NavComponent, CommonModule, MatProgressSpinnerModule],
  templateUrl: './sing-in.component.html',
  styleUrl: './sing-in.component.css'
})
export class SingInComponent {
  disabled: boolean = true;
  isLoading: boolean = false;
  incorrecLogin: boolean = false; 
  identification = "identification";
  name = "name";
  lastName = "lastName";
  email = "email";
  password = "password";
  permissions = ["WRITE", "UPDATE", "READ"];
  scopes = [2];
  
  // Form group for the login form
  loginForm = new FormGroup({
    identification: new FormControl(''), 
    name: new FormControl(''), 
    lastName: new FormControl(''), 
    email: new FormControl(''), 
    password: new FormControl(''), 
  });

  // Constructor with dependency injection
  constructor(private authService: AuthService, private router: Router,private globalService: GlobalService) {}

  // Lifecycle hook that is called after data-bound properties are initialized
  ngOnInit(): void {
    this.observeChangesInField(this.identification);
    this.observeChangesInField(this.name);
    this.observeChangesInField(this.lastName);
    this.observeChangesInField(this.email);
    this.observeChangesInField(this.password);
  }

  // Method to check the fields
  checkFields(): void {
    const identification = this.loginForm.get(this.identification)?.value;
    const name = this.loginForm.get(this.name)?.value;
    const lastName = this.loginForm.get(this.lastName)?.value;
    const emailValue = this.loginForm.get(this.email)?.value;
    const passwordValue = this.loginForm.get(this.password)?.value;

    // Disable the form if either the email or password is missing
    this.disabled = !emailValue || !passwordValue || !identification || !name || !lastName;
  }
  // observe changes in the fields (This method is mandatory in all components that have a form)
  observeChangesInField(field: string): void {
    this.loginForm.get(field)?.valueChanges.pipe(
          debounceTime(300), // Wait 300 ms after the last keystroke
          distinctUntilChanged() // Only emit distinct changes
      ).subscribe(() => {
        this.checkFields(); // Check the fields when the field changes
      });
  }
  async singUp(): Promise<void> {
    
  }

}
