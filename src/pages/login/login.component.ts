import { Component } from '@angular/core';
import { AuthService } from '../../api_connection/auth'; 
import { GlobalService } from '../../app/global.service'; 
import { FormControl, FormGroup } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms'; 
import { Router } from '@angular/router'; 
import { NavComponent } from "../general_components/nav/nav.component"; 
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'; 
import { CommonModule } from '@angular/common';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

// Component decorator with metadata
@Component({
    selector: 'app-login',
    standalone: true, 
    templateUrl: './login.component.html', 
    styleUrl: './login.component.css', 
    imports: [ReactiveFormsModule, NavComponent, CommonModule, MatProgressSpinnerModule] 
})
export class LoginComponent {
  iniciar_sesion = "Iniciar sesion"; // Test variable
  email = "email";
  password = "password";
  isLoading: boolean = false;
  disabled: boolean = true;
  incorrecLogin: boolean = false; 
  
  // Form group for the login form
  loginForm = new FormGroup({
    email: new FormControl(''), 
    password: new FormControl(''), 
  });

  // Constructor with dependency injection
  constructor(private authService: AuthService, private router: Router,private globalService: GlobalService) {}

  // Lifecycle hook that is called after data-bound properties are initialized
  ngOnInit(): void {
    this.observeChangesInField(this.email);
    this.observeChangesInField(this.password);
  }

  // Login method
  async login(): Promise<void> {
    // Create data model for the login
    const emailValue = this.loginForm.get(this.email)?.value;
    const passwordValue = this.loginForm.get(this.password)?.value;
    if (emailValue !== null && 
        emailValue !== undefined && 
        passwordValue !== null && 
        passwordValue !== undefined) {
      console.log("logeando")
      this.isLoading = true;
      const login = await this.authService.login(emailValue, passwordValue, true);
      if(login){
        console.log("logeado")
        // Change in the future
        this.globalService.setLogin(true);
        this.incorrecLogin = false;
        this.globalService.setProfileId(emailValue);
        this.router.navigate([this.globalService.getPages(1)]); 
      }else{
        this.incorrecLogin = true;
      }
      this.isLoading = false;
    }
  }

  // Method to check the fields
  checkFields(): void {
    const emailValue = this.loginForm.get(this.email)?.value;
    const passwordValue = this.loginForm.get(this.password)?.value;

    // Disable the form if either the email or password is missing
    this.disabled = !emailValue || !passwordValue;
  }
  // observe changes in the fields (This method is mandatory in all components that have a form)
  observeChangesInField(field: string): void {
    this.loginForm.get(field)?.valueChanges.pipe(
          debounceTime(300), // Wait 300 ms after the last keystroke
          distinctUntilChanged() // Only emit distinct changes
      ).subscribe(() => {
        this.checkFields(); // Check the fields when the field changes
      });
  }
}