export interface SingUpUser {
    identification: string;
    name: string;
    lastName: string;
    email: string;
    active: true;
    password: string;
    permissions: string[];
    scopes: number[];
  }