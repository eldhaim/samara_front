import { Component, Input } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { GlobalService } from '../../../app/global.service';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-nav',
  standalone: true,
  imports: [NgOptimizedImage, RouterModule, CommonModule],
  templateUrl: './nav.component.html',
  styleUrl: './nav.component.css'
})
export class NavComponent {
  constructor(private globalService: GlobalService) {}
  @Input() nombreUsuario: string | undefined;
  login: boolean = this.globalService.getLogin();
  logout() {
    //ACÁ SE DEBE EJECUTAR EL LOGIN DEL SERVIDOR COMO TAL AL TERMINAR LA SESION DEL USUARIO
    this.globalService.setLogin(false);
    this.globalService.setProfileData({});
    this.globalService.setProfileId('');
  }
}
