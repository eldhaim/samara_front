import {
    Injectable
}
from  '@angular/core'

@Injectable({ providedIn: 'root' }) export class GlobalService {
    private DEBUG: boolean = false;
    private TOKEN: string = '';
    private SERVER_TOKEN: string = '';
    private GRANT_TYPE: string = 'password';
    private API_URL: string = (!this.DEBUG)?'https://samara-api.onrender.com/API/':'http://127.0.0.1:8000/API/';
    private AUTH_URL: string =  (!this.DEBUG)?'https://samara-api.onrender.com/token':'http://127.0.0.1:8000/token';
    private AUTH_HEADERS = {'Content-type':'application/x-www-form-urlencoded', 'accept': 'application/json'};
    private API_HEADERS:object = {};
    private API_URLS = ['users/']
    //BUSCAR COMO DINAMIZAR VALIDACIONES
    private PAGES = ['/','/profile','/login']
    private LOGIN: boolean = false;
    private PROFILE_ID: string = '';
    private PROFILE_DATA: object = {};

    setToken(value:string='', user:boolean=false):void {
        if(!user){
            this.TOKEN = value;
            this.SERVER_TOKEN = value;
            this.API_HEADERS = {
              'accept':'application/json',
              'Authorization':`Bearer ${this.TOKEN}`
          }
        }else{
            this.TOKEN = value;
            this.API_HEADERS = {
              'accept':'application/json',
              'Authorization':`Bearer ${this.TOKEN}`
          }
        }
    }

    getToken():string {
        return this.TOKEN;
    }

    getGrantType():string {
        return this.GRANT_TYPE;
    }

    getApiUrl():string {
        return this.API_URL;
    }

    getAuthUrl():string {
        return this.AUTH_URL;
    }

    getAuthHeaders():object {
        return this.AUTH_HEADERS;
    }

    getApiHeaders():object {
        return this.API_HEADERS;
    }
    //validar
    getApiUrls(position: number):string {
        return this.API_URLS[position];
    }
    //validar
    getPages(position: number):string {
        return this.PAGES[position];
    }

    getLogin():boolean {
        return this.LOGIN;
    }

    setLogin(value:boolean):void {
        this.LOGIN = value;
    }

    getProfileId():string {
        return this.PROFILE_ID;
    }

    setProfileId(value:string):void {
        this.PROFILE_ID = value;
    }

    getProfileData():object {
        return this.PROFILE_DATA;
    }

    setProfileData(value:object):void {
        this.PROFILE_DATA = value;
    }
}
