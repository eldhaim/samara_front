export class Commons {
    objectIsEmpty(object: object): boolean {
        return Object.keys(object).length === 0;
    }
}
